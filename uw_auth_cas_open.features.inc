<?php

/**
 * @file
 * uw_auth_cas_open.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_auth_cas_open_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
